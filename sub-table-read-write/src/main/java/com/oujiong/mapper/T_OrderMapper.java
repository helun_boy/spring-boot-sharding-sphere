package com.oujiong.mapper;

import cn.hutool.core.date.DateTime;
import com.oujiong.entity.T_Order;
import org.apache.ibatis.annotations.Mapper;

import javax.xml.crypto.Data;
import java.util.List;

@Mapper
public interface T_OrderMapper {

    Integer insert(T_Order t_order);

    List<T_Order> selectAll(DateTime start, DateTime end);
}
