package com.oujiong.controller;


import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.oujiong.entity.T_Order;
import com.oujiong.entity.User;
import com.oujiong.service.T_OrderService;
import com.oujiong.service.UserService;
import jdk.nashorn.internal.runtime.logging.DebugLogger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.xml.crypto.Data;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * @Description: 接口测试
 *
 * @author xub
 * @date 2019/8/24 下午6:31
 */
@Slf4j
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private T_OrderService t_orderService;
    /**
     * 模拟插入数据
     */
    List<User> userList =new ArrayList<>();
    /**
     * 初始化插入数据
     */
    @PostConstruct
    private void getData() {
        userList.add(new User(100L,"小小", "女", 3));
        userList.add(new User(101L,"爸爸", "男", 30));
        userList.add(new User(102L,"妈妈", "女", 28));
        userList.add(new User(103L,"爷爷", "男", 64));
        userList.add(new User(104L,"奶奶", "女", 62));
    }
    /**
     * @Description: 批量保存用户
     */
    @PostMapping("save-user")
    public Object saveUser() {
        User user=new User("小小", "女", 3);
        int result=userService.insert(user);
        User user1=null;
        for (int i=0;i<10;i++){
             user1=userService.selectById(user.getId());
        }
        return user1;
    }
    /**
     * @Description: 获取用户列表
     */
    @GetMapping("list-user")
    public Object listUser() {
        return userService.list();
    }


    /**
     * @Description: 获取用户列表
     */
    @PostMapping("save-order")
    public Object saveOrder(@RequestBody T_Order t_order) {
        return t_orderService.insert(t_order);
    }

    /**
     * @Description: 获取用户列表
     */
    @GetMapping("add-order")
    public List<T_Order> addOrder() {
        ExecutorService executor = Executors.newFixedThreadPool(5);
        List<T_Order> list=new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            executor.submit(() -> {
                System.out.println("thread id is: " + Thread.currentThread().getId());
                T_Order t_order=new T_Order();
                Random rd=new Random();
                t_order.setName("name"+(rd.nextInt(12)+1));
                Date now=new Date();
                String createTime="2020-"+(rd.nextInt(12)+1)+"-"+(rd.nextInt(12)+1)+" "+DateUtil.format(now,"HH:mm:ss");
                DateTime createDate=DateUtil.parse(createTime,"yyyy-MM-dd HH:mm:ss");
//                t_order.setGmtCreate(createDate);
                t_orderService.insert(t_order);
                list.add(t_order);
                log.info("t_order---:"+t_order.getId());
            });
        }
        return list;
    }


    /**
     * @Description: 获取用户列表
     */
    @GetMapping("list-order")
    public Object listOrder() {

        Date now=new Date();
        Random rd=new Random();
        String createTime="2020-"+(rd.nextInt(12)+1)+"-"+(rd.nextInt(12)+1)+" "+DateUtil.format(now,"HH:mm:ss");
        DateTime start= DateUtil.parseDateTime(createTime);
        String enbTime="2020-"+(rd.nextInt(12)+1)+"-"+(rd.nextInt(12)+1)+" "+DateUtil.format(now,"HH:mm:ss");
        DateTime end= DateUtil.parseDateTime(enbTime);
        List<T_Order> list=t_orderService.selectAll(start,end);
        log.info(list.size()+"");
        return list;
    }
}
