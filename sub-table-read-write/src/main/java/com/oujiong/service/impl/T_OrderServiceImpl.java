package com.oujiong.service.impl;

import cn.hutool.core.date.DateTime;
import com.oujiong.entity.T_Order;
import com.oujiong.mapper.T_OrderMapper;
import com.oujiong.service.T_OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.crypto.Data;
import java.util.List;

@Service
public class T_OrderServiceImpl implements T_OrderService {

    @Autowired
    private T_OrderMapper t_orderMapper;

    @Override
    public Integer insert(T_Order t_order) {
        return t_orderMapper.insert(t_order);
    }

    @Override
    public List<T_Order> selectAll(DateTime start, DateTime end) {
        List<T_Order> list=t_orderMapper.selectAll(start,end);
        return list;
    }



}
