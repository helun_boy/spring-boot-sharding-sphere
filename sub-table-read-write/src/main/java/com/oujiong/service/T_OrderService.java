package com.oujiong.service;

import cn.hutool.core.date.DateTime;
import com.oujiong.entity.T_Order;
import com.oujiong.entity.User;

import javax.xml.crypto.Data;
import java.util.List;

public interface T_OrderService {
    Integer insert(T_Order t_order);
    List<T_Order> selectAll(DateTime start, DateTime end);
}
