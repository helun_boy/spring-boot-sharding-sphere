package com.oujiong.config;

import org.apache.shardingsphere.api.sharding.standard.RangeShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.RangeShardingValue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MyRangeShardingAlgorithm implements RangeShardingAlgorithm {

    @Override
    public Collection<String> doSharding(Collection collection, RangeShardingValue rangeShardingValue) {
        List<String> shardingSuffix = new ArrayList<>();
        shardingSuffix.add("2020");
        return shardingSuffix;
    }
}
