package com.oujiong.config;


import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Range;
import lombok.extern.slf4j.Slf4j;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;
import org.apache.shardingsphere.api.sharding.standard.RangeShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.RangeShardingValue;

import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
public class LogShardingAlgorithm implements PreciseShardingAlgorithm<Date>, RangeShardingAlgorithm<Date> {

    @Override
    public String doSharding(Collection<String> availableTargetNames, PreciseShardingValue<Date> shardingValue) {
        // 根据配置的分表规则生成目标表的后缀
        String tableExt = DateUtil.format(shardingValue.getValue(), "yyyyMM");
        Iterator iterator = availableTargetNames.iterator();
        String tableName=iterator.next()+tableExt;
        return tableName;
    }

    @Override
    public Collection<String> doSharding(Collection<String> availableTargetNames, RangeShardingValue<Date> shardingValue) {
        Collection<String> availables = new ArrayList<>();
        Range valueRange = shardingValue.getValueRange();
        for (String target : availableTargetNames) {

            Date lowerEnd = (Date) valueRange.lowerEndpoint();
            Date upperEnd = (Date) valueRange.upperEndpoint();
            String lt=((RangeShardingValue<Date>) shardingValue).getLogicTableName();
            availables = getRoutTable(lt, lowerEnd, upperEnd);

        }
        return availables;
    }

    private Collection<String> getRoutTable(String logicTable, Date lowerEnd, Date upperEnd) {
        Set<String> routTables = new HashSet<String>();
        if (lowerEnd != null && upperEnd != null) {
            List<String> rangeNameList = getRangeNameList(lowerEnd, upperEnd);
            for (String string : rangeNameList) {
                routTables.add(logicTable +"_"+ string);
            }
        }
        return routTables;
    }

    private static List<String> getRangeNameList(Date start, Date end) {
        List<String> result = Lists.newArrayList();
        Calendar dd = Calendar.getInstance();// 定义日期实例

        dd.setTime(start);// 设置日期起始时间

        while (dd.getTime().before(end)) {// 判断是否到结束日期

            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");

            String str = sdf.format(dd.getTime());

            result.add(str);

            dd.add(Calendar.MONTH, 1);// 进行当前日期月份加1
        }
        return result;
    }

}
