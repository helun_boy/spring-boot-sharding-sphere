package com.oujiong.config;



import cn.hutool.core.date.DateUtil;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

public class MonthTableShardingAlgorithm implements PreciseShardingAlgorithm<Date> {

    /**
     * 等值查询分表路由策略, 根据传入date返回响应以年月结尾的表
     * @param availableTargetNames 可用表名
     * @param shardingValue 分片条件
     * @return 符合分片条件的表名
     */
    @Override
    public String doSharding(Collection<String> availableTargetNames, PreciseShardingValue<Date> shardingValue) {
        // 根据配置的分表规则生成目标表的后缀
        String tableExt = DateUtil.format(shardingValue.getValue(), "yyyyMM");
        Iterator iterator = availableTargetNames.iterator();
        String tableName=iterator.next()+tableExt;
        return tableName;
//        for (String availableTableName : availableTargetNames) {
//            if (availableTableName.endsWith(tableExt)) {
//                return availableTableName;
//            }
//        }
//        return null;
    }

}
