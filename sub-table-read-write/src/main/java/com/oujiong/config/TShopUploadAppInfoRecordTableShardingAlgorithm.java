package com.oujiong.config;

import cn.hutool.core.date.DateUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Range;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.shardingsphere.api.sharding.ShardingValue;
import org.apache.shardingsphere.api.sharding.complex.ComplexKeysShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.complex.ComplexKeysShardingValue;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;
import org.apache.shardingsphere.api.sharding.standard.RangeShardingValue;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
public class TShopUploadAppInfoRecordTableShardingAlgorithm implements ComplexKeysShardingAlgorithm  {


    @SuppressWarnings("unchecked")
    @Override
    public Collection<String> doSharding(Collection collection, ComplexKeysShardingValue complexKeysShardingValue) {
        log.info("自定义按照日期进行分表");
        List<String> shardingSuffix = new ArrayList<>();
        Collection<String> routTables = new HashSet<String>();
        //获取分表字段及字段值
        Map<String, Collection<Date>> map = complexKeysShardingValue.getColumnNameAndShardingValuesMap();
        Map<String, Collection<ShardingValue>> mapShardingValue = complexKeysShardingValue.getColumnNameAndShardingValuesMap();
        //获取字段值
        Collection<Date> shardingValues = map.get("gmt_create");

        if (mapShardingValue != null) {
            for (ShardingValue shardingValue : mapShardingValue.get("gmt_create")) {

                // eq 条件
                if (shardingValue instanceof PreciseShardingValue) {
                    PreciseShardingValue<Date> preciseShardingValue = (PreciseShardingValue<Date>) shardingValue;

                    Date value = preciseShardingValue.getValue();
                    String routTable = getRoutTable(preciseShardingValue.getLogicTableName(), value);
                    if ((routTable!=null)) {
                        routTables.add(routTable);
                    }
                    // between 条件
                } else if (shardingValue instanceof RangeShardingValue) {
                    RangeShardingValue<Date> rangeShardingValue = (RangeShardingValue<Date>) shardingValue;
                    Range<Date> valueRange = rangeShardingValue.getValueRange();
                    Date lowerEnd = (Date) valueRange.lowerEndpoint();
                    Date upperEnd = (Date) valueRange.upperEndpoint();
                    String lt=((RangeShardingValue<Date>) shardingValue).getLogicTableName();
                    Collection<String> tables = getRoutTable(lt, lowerEnd, upperEnd);
                    if (tables != null && tables.size() > 0) {
                        routTables.addAll(tables);
                    }

                }

                if (routTables != null && routTables.size() > 0) {
                    return routTables;
                }
            }
        }


        if (!CollectionUtils.isEmpty(shardingValues)) {
            for (Date date : shardingValues) {
                //获取日期时间所在的月份
                String str = DateUtil.format(date, "yyyyMM");
                //添加记录所在分表表名集合
                shardingSuffix.add(complexKeysShardingValue.getLogicTableName() + "_" + str);
            }
        }

        return shardingSuffix;

    }

    private String getRoutTable(String logicTable, Date keyValue) {
        if (keyValue != null) {
            String formatDate = DateUtil.format(keyValue, "yyyyMM");
            return logicTable + formatDate;
        }
        return null;

    }

    private Collection<String> getRoutTable(String logicTable, Date lowerEnd, Date upperEnd) {
        Set<String> routTables = new HashSet<String>();
        if (lowerEnd != null && upperEnd != null) {
            List<String> rangeNameList = getRangeNameList(lowerEnd, upperEnd);
            for (String string : rangeNameList) {
                routTables.add(logicTable + string);
            }
        }
        return routTables;
    }

    private static List<String> getRangeNameList(Date start, Date end) {
        List<String> result = Lists.newArrayList();
        Calendar dd = Calendar.getInstance();// 定义日期实例

        dd.setTime(start);// 设置日期起始时间

        while (dd.getTime().before(end)) {// 判断是否到结束日期

            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");

            String str = sdf.format(dd.getTime());

            result.add(str);

            dd.add(Calendar.MONTH, 1);// 进行当前日期月份加1
        }
        return result;
    }



}
